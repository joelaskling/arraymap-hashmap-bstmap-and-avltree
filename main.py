from Visualizer import visualize
from Experiment import experiment_runner

max_n = 50000
step_size = 5000
hash_size = max_n

results, num_key_lst, max_load_factor = experiment_runner.run(max_n, step_size, hash_size)
for i, metod in enumerate(results['list_name']):
    visualize.plot_results(results, num_key_lst, metod, i, max_load_factor)


