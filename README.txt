ADT:
array map:
Uses binary search for get(), contains() och delete().

hash map:
I choosed to implement open hashing.

bst map:
Standard binary search tree, uses successor.

AVLtree:
The root is self, it can't be deleted therefore. But the key and value are set to None. The hight is used on every node to calculate the balance factor

Experiment:
The initial amount of keys were set to 20.000 with a stepsize of 1000, and 50.000 keys with a stepsize of 5000. The result was as aspected.
I thought the AVL-tree should be a bit faster regarding the get() and contain() functions.
