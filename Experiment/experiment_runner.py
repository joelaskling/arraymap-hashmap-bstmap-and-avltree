import time
from adt_classes import array_map
from adt_classes import balance_bst_map
from adt_classes import bst_map
from adt_classes import hash_map
from Data import test_data_generator


def get_time_put(data, adt):
    start = time.clock()
    for key in data:
        adt.put(key, None)
    end = time.clock()
    return end - start


def get_time_get(data, adt):
    start = time.clock()
    for key in data:
        adt.get(key)
    end = time.clock()
    return end - start


def get_time_contains(data, adt):
    start = time.clock()
    for key in data:
        adt.contains(key)
    end = time.clock()
    return end - start


def get_time_delete(data, adt):
    start = time.clock()
    for key in data:
        adt.delete(key)
    end = time.clock()
    return end - start


def run(max_n, step_size, hash_size):
    max_load_factor = []
    map_array = array_map.ArrayMap()
    map_hash = hash_map.HashMap(hash_size, hash_map.hash_function)
    map_bst = bst_map.BSTMap()
    avl_tree = balance_bst_map.AVLTree()
    num_key_lst = []

    results = {}
    results['ADT'] = [map_array, map_hash, map_bst, avl_tree]
    results['ADT_name'] = ['map_array', 'map_hash', 'map_bst', 'avl_tree']
    results['metod_name'] = ['get_time_put', 'get_time_get', 'get_time_contains', 'get_time_delete']
    results['list_name'] = ['put_time', 'get_time', 'contains_time', 'delete_time']
    results['ADTrandom'] = {'map_array':
                            {'put_time': [], 'get_time': [], 'delete_time': [], 'contains_time': []},
                            'map_hash':
                            {'put_time': [], 'get_time': [], 'delete_time': [], 'contains_time': []},
                            'map_bst':
                            {'put_time': [], 'get_time': [], 'delete_time': [], 'contains_time': []},
                            'avl_tree':
                            {'put_time': [], 'get_time': [], 'delete_time': [], 'contains_time': []}
                            }
    results['ADTsorted'] = {'map_array':
                                {'put_time': [], 'get_time': [], 'delete_time': [], 'contains_time': []},
                            'map_hash':
                                {'put_time': [], 'get_time': [], 'delete_time': [], 'contains_time': []},
                            'map_bst':
                                {'put_time': [], 'get_time': [], 'delete_time': [], 'contains_time': []},
                            'avl_tree':
                                {'put_time': [], 'get_time': [], 'delete_time': [], 'contains_time': []}
                            }
    results['metods'] = {'get_time_put': get_time_put,
                         'get_time_get': get_time_get,
                         'get_time_contains': get_time_contains,
                         'get_time_delete': get_time_delete}
    for adt_index in range(len(results['ADT_name'])):
        for metod_index in range(len(results['metod_name'])):
            num_key_lst = []
            for num_keys in range(step_size, max_n + 1, step_size):
                random_data = test_data_generator.random_keys(num_keys, num_keys)
                num_key_lst.append(num_keys)

                metod = results['metod_name'][metod_index]
                time_lst = results['list_name'][metod_index]
                adt_name = results['ADT_name'][adt_index]

                print('Random keys: ', adt_name, ' ', metod, ' ', num_keys)

                results['ADTrandom'][adt_name][time_lst].append(results['metods'][metod](random_data,
                                                                                         results['ADT'][adt_index]))
                if adt_name == 'map_hash' and metod == 'get_time_put' and num_keys == max_n:
                    max_load_factor.append(map_hash.load_factor)

            for num_keys in range(step_size, max_n + 1, step_size):
                incrementing_data = test_data_generator.incr_order_keys(num_keys)

                metod = results['metod_name'][metod_index]
                time_lst = results['list_name'][metod_index]
                adt_name = results['ADT_name'][adt_index]

                print('Incrementing keys: ', adt_name, ' ', metod, ' ', num_keys)

                results['ADTsorted'][adt_name][time_lst].append(results['metods'][metod](incrementing_data,
                                                                                         results['ADT'][adt_index]))
                if adt_name == 'map_hash' and metod == 'get_time_put' and num_keys == max_n:
                    max_load_factor.append(map_hash.load_factor)

    return results, num_key_lst, max_load_factor
