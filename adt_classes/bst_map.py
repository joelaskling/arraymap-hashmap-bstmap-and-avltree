class BSTMap:
    def __init__(self):
        self.key, self.value = None, None
        self.right, self.left, self.parent = None, None, None

    def put(self, key, val):
        if not self.key:
            self.key, self.value = key, val
        elif key < self.key:
            if not self.left:
                self.left = BSTMap()
                self.left.parent = self
            self.left.put(key, val)
        elif key > self.key:
            if not self.right:
                self.right = BSTMap()
                self.right.parent = self
            self.right.put(key, val)
        else:
            self.value = val

    def get(self, key):
        if self.key == key:
            return self.value
        elif not (self.right and self.left):
            return None
        elif key < self.key:
            return self.left.get(key)
        else:
            return self.right.get(key)

    def delete(self, key, right=False):
        if not self.key:
            return
        if self.key and key < self.key:
            if self.left:
                return self.left.delete(key)
        elif self.key and key > self.key:
            if self.right:
                right = True
                return self.right.delete(key, right)
        elif self.key:
            if not (self.left or self.right):
                if not right:
                    self.parent.left = None
                else:
                    self.parent.right = None
            elif self.left and not self.right:
                self.key, self.value = self.left.key, self.left.value
                self.left, self.right = self.left.left, self.left.right
            elif self.right and not self.left:
                self.key, self.value = self.right.key, self.right.value
                self.left, self.right = self.right.left, self.right.right
            else:
                successor, right = self.right.__find_successor()
                self.key, self.value = successor.key, successor.value
                successor.delete(successor.key, right=right)

    def __find_successor(self):
        if self.left:
            return self.left.__find_successor()
        if self.key > self.parent.key:
            right = True
        else:
            right = False
        return self, right

    def length(self):
        if self.left:
            left_size = self.left.length()
        else:
            left_size = 0
        if self.right:
            right_size = self.right.length()
        else:
            right_size = 0
        return left_size + 1 + right_size

    def contains(self, key):
        if self.key == key:
            return True
        elif not (self.right and self.left):
            return False
        elif key < self.key:
            return self.left.contains(key)
        else:
            return self.right.contains(key)






