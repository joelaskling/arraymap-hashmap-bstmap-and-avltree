class AVLTree:
    def __init__(self):
        self.key, self.value = None, None
        self.right, self.left, self.parent = None, None, None
        self.balance = 0
        self.height = 1

    def display(self):
        lines, _, _, _ = self._display_aux()
        for line in lines:
            print(line)

    def _display_aux(self):
        """Returns list of strings, width, height, and horizontal coordinate of the root."""
        # No child.
        if self.right is None and self.left is None:
            line = 'k:%d b:%d h:%d' % (self.key, self.balance, self.height)
            width = len(line)
            height = 1
            middle = width // 2
            return [line], width, height, middle

        # Only left child.
        if self.right is None:
            lines, n, p, x = self.left._display_aux()
            s = 'k:%s b:%s h:%s' % (self.key, self.balance, self.height)
            u = len(s)
            first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s
            second_line = x * ' ' + '/' + (n - x - 1 + u) * ' '
            shifted_lines = [line + u * ' ' for line in lines]
            return [first_line, second_line] + shifted_lines, n + u, p + 2, n + u // 2

        # Only right child.
        if self.left is None:
            lines, n, p, x = self.right._display_aux()
            s = 'k:%s b:%s h:%s' % (self.key, self.balance, self.height)
            u = len(s)
            first_line = s + x * '_' + (n - x) * ' '
            second_line = (u + x) * ' ' + '\\' + (n - x - 1) * ' '
            shifted_lines = [u * ' ' + line for line in lines]
            return [first_line, second_line] + shifted_lines, n + u, p + 2, u // 2

        # Two children.
        left, n, p, x = self.left._display_aux()
        right, m, q, y = self.right._display_aux()
        s = 'k: %d b: %d h: %d' % (self.key, self.balance, self.height)
        u = len(s)
        first_line = (x + 1) * ' ' + (n - x - 1) * '_' + s + y * '_' + (m - y) * ' '
        second_line = x * ' ' + '/' + (n - x - 1 + u + y) * ' ' + '\\' + (m - y - 1) * ' '
        if p < q:
            left += [n * ' '] * (q - p)
        elif q < p:
            right += [m * ' '] * (p - q)
        zipped_lines = zip(left, right)
        lines = [first_line, second_line] + [a + u * ' ' + b for a, b in zipped_lines]
        return lines, n + m + u, max(p, q) + 2, n + u // 2

    def __update_height_and_balance(self, delete=False):
        if abs(self.balance) > 1:
            if delete:
                if not self.parent:
                    delete = False
                    self.__rebalance()
                else:
                    self.__rebalance()
            else:
                self.__rebalance()
                return
        if self.parent:
            if self.parent.left and self.parent.right:
                self.parent.height = max(self.parent.left.height, self.parent.right.height) + 1
                self.parent.balance = self.parent.left.height - self.parent.right.height
            elif not self.parent.left and self.parent.right:
                self.parent.height = self.parent.right.height + 1
                self.parent.balance = 0 - self.parent.right.height
            else:
                self.parent.height = self.parent.left.height + 1
                self.parent.balance = self.parent.left.height
            return self.parent.__update_height_and_balance()

    def put(self, key, val):
        if not self.key:
            self.key, self.value = key, val
            self.__update_height_and_balance()
        elif key < self.key:
            if not self.left:
                self.left = AVLTree()
                self.left.parent = self
            self.left.put(key, val)
        elif key > self.key:
            if not self.right:
                self.right = AVLTree()
                self.right.parent = self
            self.right.put(key, val)
        else:
            self.value = val

    def get(self, key):
        if self.key == key:
            return self.value
        elif not (self.right and self.left):
            return None
        elif key < self.key:
            return self.left.get(key)
        else:
            return self.right.get(key)

    def delete(self, key, right=False):
        if not self.parent and self.height == 1:
            self.key, self.value = None, None
            return
        if self.key and key < self.key:
            if self.left:
                return self.left.delete(key)
        elif self.key and key > self.key:
            if self.right:
                right = True
                return self.right.delete(key, right)
        elif self.key:
            if not (self.left or self.right):
                if not right:
                    self.parent.left = None
                    if self.key and self.parent:
                        self.parent.correct_balance()
                        self.parent.__update_height_and_balance(True)
                else:
                    self.parent.right = None
                    if self.key and self.parent:
                        self.parent.correct_balance()
                        self.parent.__update_height_and_balance(True)
            elif self.left and not self.right:
                self.key, self.value = self.left.key, self.left.value
                self.left, self.right = self.left.left, self.left.right
                self.correct_balance()
                if self.key and self.parent:
                    self.__update_height_and_balance(True)
            elif self.right and not self.left:
                self.key, self.value = self.right.key, self.right.value
                self.left, self.right = self.right.left, self.right.right
                self.correct_balance()
                if self.key and self.parent:
                    self.__update_height_and_balance(True)
            else:
                successor, right = self.right.__find_successor()
                self.key, self.value = successor.key, successor.value
                successor.delete(successor.key, right=right)

    def length(self):
        if self.left:
            left_size = self.left.length()
        else:
            left_size = 0
        if self.right:
            right_size = self.right.length()
        else:
            right_size = 0
        return left_size + 1 + right_size

    def contains(self, key):
        if self.key == key:
            return True
        elif not (self.right and self.left):
            return False
        elif key < self.key:
            return self.left.contains(key)
        else:
            return self.right.contains(key)

    def __find_successor(self):
        if self.left:
            return self.left.__find_successor()
        if self.key > self.parent.key:
            right = True
        else:
            right = False
        return self, right

    def __rebalance(self):
        if self.balance < 0:
            if self.right.balance > 0:
                self.right.__rightRotation(True)
            else:
                self.__leftRotation()
        elif self.balance > 0:
            if self.left.balance < 0:
                self.left.__leftRotation(True)
            else:
                self.__rightRotation()

    def correct_balance(self):
        if self.left and self.right:
            self.height = max(self.left.height, self.right.height) + 1
            self.balance = self.left.height - self.right.height
        elif self.left and not self.right:
            self.height = self.left.height + 1
            self.balance = self.left.height
        elif self.right and not self.left:
            self.height = self.right.height + 1
            self.balance = 0 - self.right.height
        else:
            self.balance = 0
            self.height = 1

    def __rightRotation(self, rotate_left=False):
        tmp_key, tmp_value = self.key, self.value
        top = self
        mid = self.left
        bot = self.left.left

        top.key, top.value = mid.key, mid.value
        mid.key, mid.value = tmp_key, tmp_value
        if bot:
            bot.parent = top
        top.left = bot
        mid.left = mid.right
        mid.right = top.right
        top.right = mid
        if mid.right:
            mid.right.parent = mid

        mid.correct_balance()
        top.correct_balance()

        if rotate_left:
            self.parent.__leftRotation()

    def __leftRotation(self, rotate_right=False):
        tmp_key, tmp_value = self.key, self.value
        top = self
        mid = self.right
        bot = self.right.right

        top.key, top.value = mid.key, mid.value
        mid.key, mid.value = tmp_key, tmp_value
        if bot:
            bot.parent = top
        top.right = bot
        mid.right = mid.left
        mid.left = top.left
        top.left = mid
        if mid.left:
            mid.left.parent = mid

        mid.correct_balance()
        top.correct_balance()

        if rotate_right:
            self.parent.__rightRotation()

