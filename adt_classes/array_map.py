class ArrayMap:
    def __init__(self):
        self.arr_map = []

    def put(self, key, val):
        item = (key, val)
        if not self.arr_map:
            self.arr_map.append(item)
        else:
            for i in range(len(self.arr_map)):
                if key == self.arr_map[i][0]:
                    self.arr_map[i] = item
                    break
                elif key < self.arr_map[i][0]:
                    self.arr_map.insert(i, item)
                    break
                elif i == len(self.arr_map) - 1:
                    self.arr_map.append(item)

    def get(self, key):
        if self.arr_map:
            index = self.__binary_search(key)
            if index != None:
                return self.arr_map[index][1]
        return None

    def delete(self, key):
        if self.arr_map:
            index = self.__binary_search(key)
            if index != None:
                self.arr_map.pop(index)

    def length(self):
        return len(self.arr_map)

    def contains(self, key):
        if self.arr_map:
            index = self.__binary_search(key)
            if index != None:
                return True
        return False

    def __binary_search(self, key):
        first = 0
        last = len(self.arr_map) - 1
        found = None
        while first <= last:
            midpoint = (first + last) // 2
            if self.arr_map[midpoint][0] == key:
                found = midpoint
                break
            else:
                if key < self.arr_map[midpoint][0]:
                    last = midpoint - 1
                else:
                    first = midpoint + 1
        return found
