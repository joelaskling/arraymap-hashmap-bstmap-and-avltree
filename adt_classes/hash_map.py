class HashMap:
    def __init__(self, size, hash_func):
        self.hash_func, self.size = hash_func, size
        self.items = [[] for _ in range(self.size)]
        self.num_items = 0
        self.load_factor = 0

    def put(self, key, val):
        item = (key, val)
        hash_value = self.hash_func(key, self.size)
        bucket = self.items[hash_value]
        check = True

        if not bucket:
            bucket.append(item)
            self.num_items += 1
            check = False
        else:
            for i, (k, v) in enumerate(bucket):
                if k == key:
                    bucket[i] = (key, val)
                    check = False
        if check:
            bucket.append(item)
            self.num_items += 1
        self.load_factor = (self.num_items / self.size) * 100

    def get(self, key):
        hash_value = hash_function(key, self.size)
        bucket = self.items[hash_value]

        if bucket:
            for (k, v) in bucket:
                if k == key:
                    return v
        return None

    def delete(self, key):
        hash_value = hash_function(key, self.size)
        bucket = self.items[hash_value]

        if bucket:
            for i, (k, v) in enumerate(bucket):
                if k == key:
                    bucket.pop(i)
                    self.num_items -= 1
                    break
        self.load_factor = (self.num_items / self.size) * 100

    def length(self):
        return self.num_items

    def contains(self, key):
        hash_value = hash_function(key, self.size)
        bucket = self.items[hash_value]

        if bucket:
            for (k, v) in bucket:
                if k == key:
                    return True
        return False


def hash_function(key, size):
    return key % size
