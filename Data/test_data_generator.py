import random


def random_keys(num_keys, end):
    rand_num = []
    for i in range(num_keys):
        rand_num.append(random.randint(1, end))
    return rand_num


def incr_order_keys(num_keys):
    rand_num = []
    for i in range(num_keys):
        rand_num.append(i)
    return rand_num


def decr_order_keys(num_keys):
    rand_num = []
    for i in range(num_keys, 1, -1):
        rand_num.append(i)
    return rand_num