import matplotlib.pyplot as plt


def plot_results(results, num_key_lst, metod, i, max_load_factor):

    x = num_key_lst

    t1 = results['ADTrandom']['map_array'][metod]
    t2 = results['ADTrandom']['map_hash'][metod]
    t3 = results['ADTrandom']['map_bst'][metod]
    t4 = results['ADTrandom']['avl_tree'][metod]

    y1 = results['ADTsorted']['map_array'][metod]
    y2 = results['ADTsorted']['map_hash'][metod]
    y3 = results['ADTsorted']['map_bst'][metod]
    y4 = results['ADTsorted']['avl_tree'][metod]

    fig, axes = plt.subplots(1, 2)

    axes[0].plot(x, t1, label=results['ADT_name'][0])
    axes[0].plot(x, t2, label=results['ADT_name'][1] + ' max_load = ' + str(int(max_load_factor[0])) + '%')
    axes[0].plot(x, t3, label=results['ADT_name'][2])
    axes[0].plot(x, t4, label=results['ADT_name'][3])

    axes[1].plot(x, y1, label=results['ADT_name'][0])
    axes[1].plot(x, y2, label=results['ADT_name'][1] + ' max_load = ' + str(int(max_load_factor[1])) + '%')
    axes[1].plot(x, y3, label=results['ADT_name'][2])
    axes[1].plot(x, y4, label=results['ADT_name'][3])

    axes[0].set_xlabel('Number of keys')
    axes[0].set_ylabel('Time')

    axes[0].set_title('Random keys')
    axes[1].set_title('Incrementing keys')

    fig.suptitle(metod)

    axes[0].legend()
    axes[1].legend()
    plt.savefig(results['list_name'][i] + '.png')
    plt.show()